import { Sucursal, Empresa } from "../models/index.js";

export const createSucursal = async (request, response) => {
    try {
        const body = request.body;
    
        if (body.nombre == null || typeof body.nombre != "string") {
            errorHelper(response, 400, 'El nombre es obligatorio y debe ser un string');
            return;
        }
        
        if (body.empresaId == null || typeof body.empresaId != "number") {
            errorHelper(response, 400, 'El id de la empresa es obligatorio y debe ser un entero');
            return;
        }

        const whereEmpresa = { 
            where: { 
                id: body.empresaId 
            }
        };
    
        const empresa = await Empresa.findOne(whereEmpresa);

        if (!empresa) {
            errorHelper(response, 404, 'La empresa no existe');
            return;
        }

        const nuevaSucursal = {
            nombre: body.nombre,
            direccion: body.direccion,
            gerente: body.gerente,
            personal: body.personal,
            empresa_id: body.empresaId,
        };

        await Sucursal.create(nuevaSucursal);

        response
            .status(200)
            .send({ 
                success: true,
                message: "Sucursal creada con exito" 
            });

    } catch (error) {
        console.log(error);
    }
}

export const updateSucursalById = async (request, response) => {
    try{
        const idSucursal = request.params.id;
    
        const filtro = { 
            where: { 
                id: idSucursal
            }
        };
    
        const sucursal = await Sucursal.findOne(filtro)
        
        if (!sucursal) {
            errorHelper(response, 404, 'La sucursal no existe');
            return;
        }

        const body = request.body;

        if (body.nombre == null || typeof body.nombre != "string" || body.nombre == "") {
            errorHelper(response, 400, 'El nombre es obligatorio y debe ser un string');
            return;
        }

        const nuevoBody = {
            nombre: body.nombre
        };

        if (body.direccion != null && typeof body.direccion == "string") {
            nuevoBody.direccion = body.direccion;
        }

        if (body.gerente != null && typeof body.gerente == "string") {
            nuevoBody.gerente = body.gerente;
        }

        if (body.personal != null && typeof body.personal == "number") {
            nuevoBody.personal = body.personal;
        }

        await Sucursal.update(nuevoBody, filtro);
                
        response
            .status(200)
            .send({
                success: true,
                mensaje: "La sucursal se actualizó con éxito"
            });
    } catch (error) {
        errorHelper(response, 500, error.message || 'Algo salió mal');
    }
};

export const getSucursalById = async (request, response) => {
    try{
        const idEmpresa = request.params.id;
    
        const filtro = { 
            where: { 
                id: idEmpresa 
            },
            include: ["empresa"]
        };
    
        const sucursal = await Sucursal.findOne(filtro)
        
        if (sucursal) {
            response.send(sucursal)
        } else {
            response
                .status(404)
                .send({
                    mensaje: 'La empresa no existe'
                });
        }
    } catch (error) {
        response
            .status(500)
            .send({
                mensaje: error.message || 'Algo salió mal'
            })
    }
};

export const deleteSucursalById = async (request, response) => {
    try {
        const id = request.params.id;
        console.log(id);
        if (!id || isNaN(id)) {
            errorHelper(response, 400, "El id no es valido");
            return;
        }

        const filtro = {
            where: {
                id: id
            },
        };

        const sucursal = await Sucursal.findOne(filtro);

        if (!sucursal) {
            errorHelper(response, 404, "No existe la sucursal");
            return;
        }

        await Sucursal.destroy(filtro);

        response
            .status(200)
            .send({
                mensaje: "La sucursal se elimino correctamente",
                success: true
            });
    } catch (error) {
        errorHelper(response, 500, error.message);
        return;
    }
}

const errorHelper = (response, codigo, mensaje) => {
    response
        .status(codigo)
        .send({
            success: false,
            mensaje: mensaje
        });
}