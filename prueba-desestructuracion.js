let nombres = ["pablo", "aldana", "nerea", "bruno", "david"];
let [nombre1, nombre2, ...restoNombres] = nombres;

// console.log(nombre1);
// console.log(nombre2);
// console.log(restoNombres);

let casa = {
    habitaciones: {
        cantidad: 2,
        espacio: "60 mt2"
    },
    // baños: 1,
    patio: false,
    cocina: true,
    escalera: false
};

let { 
    habitaciones: {
        cantidad: numero,
        espacio
    },
    baños = 0
} = casa;


console.log(baños);
// console.log(espacio);