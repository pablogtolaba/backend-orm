import { INTEGER, STRING } from 'sequelize';
import db from '../database-connection.js';

const nombre = 'empresas';

const modelo = {
    id: {
        type: INTEGER, 
        primaryKey: true
    },
    nombre: {
        type: STRING
    },
    cuit: {
        type: STRING
    }
};

const opcional = {
    timestamps: false,
    freezeTableName: true
};

const Empresa = db.define(nombre, modelo, opcional);

export default Empresa;