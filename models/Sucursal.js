import { INTEGER, STRING } from 'sequelize';
import db from '../database-connection.js';

const Sucursal = db.define('sucursales', {
    id: {
        type: INTEGER,
        primaryKey: true
    },
    nombre: STRING,
    direccion: STRING,
    gerente: STRING,
    personal: INTEGER,
}, {
    timestamps: false
});

export default Sucursal;
