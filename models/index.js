import Empresa from "./Empresa.js";
import Sucursal from "./Sucursal.js";

Empresa.hasMany(Sucursal, { 
    foreignKey: 'empresa_id',
    as : "sucursales"
});

Sucursal.belongsTo(Empresa, {
    foreignKey: 'empresa_id',
    as: 'empresa'
});

export { Empresa, Sucursal };